package com.atlassian.devops.stash;

import com.atlassian.devops.pull.Commit;
import com.atlassian.devops.pull.PullRequest;
import com.atlassian.devops.pull.PullRequestActivity;
import com.atlassian.devops.util.Page;
import com.google.common.base.Charsets;
import com.google.common.io.Files;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class StashResponseParserTest {

    private StashResponseParser stashResponseParser = new StashResponseParser();

    @Test
    public void testShouldParsePullRequestListData() {
        String pullRequestResults = readTestResource("stash-pullrequests-result.json");

        Page<PullRequest> page = stashResponseParser.parsePullRequestList(pullRequestResults);

        assertNotNull(page);
        assertEquals(12, page.getStart());
        assertEquals(5, page.getLimit());
        assertEquals(5, page.getSize());
        assertFalse(page.isLastPage());

        List<PullRequest> pullRequests = page.getValues();
        assertNotNull(pullRequests);
        assertEquals(5, pullRequests.size());
        assertEquals(12911, pullRequests.get(0).getPullRequestId());
        assertEquals(12932, pullRequests.get(1).getPullRequestId());
        assertEquals(12927, pullRequests.get(2).getPullRequestId());
        assertEquals(12919, pullRequests.get(3).getPullRequestId());
        assertEquals(12853, pullRequests.get(4).getPullRequestId());
    }

    @Test
    public void testShouldParsePullRequestActivitesListData() {
        String activityResults = readTestResource("stash-activities-result.json");

        Page<PullRequestActivity> page = stashResponseParser.parseActivityList(activityResults);

        assertNotNull(page);
        assertEquals(0, page.getStart());
        assertEquals(25, page.getLimit());
        assertEquals(6, page.getSize());
        assertTrue(page.isLastPage());

        List<PullRequestActivity> activities = page.getValues();
        assertNotNull(activities);
        assertEquals(6, activities.size());
        assertEquals("MERGED", activities.get(0).getAction());
        assertEquals("COMMENTED", activities.get(1).getAction());
        assertEquals("COMMENTED", activities.get(2).getAction());
        assertEquals("APPROVED", activities.get(3).getAction());
        assertEquals("REVIEWED", activities.get(4).getAction());
        assertEquals("OPENED", activities.get(5).getAction());
    }

    @Test
    public void testShouldParseCommitListData() {
        String commitResults = readTestResource("stash-commits-result.json");

        Page<Commit> page = stashResponseParser.parseCommitList(commitResults);

        assertNotNull(page);
        assertEquals(8, page.getStart());
        assertEquals(5, page.getLimit());
        assertEquals(3, page.getSize());
        assertTrue(page.isLastPage());

        List<Commit> commits = page.getValues();
        assertNotNull(commits);
        assertEquals(3, commits.size());
        assertEquals("1883b53b8d0646efa96d3b0b5935cddc3c5acd0d", commits.get(0).getCommitId());
        assertEquals(Date.from(Instant.ofEpochMilli(1543391013000L)), commits.get(0).getAuthorTimestamp());
        assertEquals("f904d097d58a7801580ad73a3e18f464f793320c", commits.get(1).getCommitId());
        assertEquals("b3c3bc708f8770c5c032296c62127516ce0d4f38", commits.get(2).getCommitId());
    }

    private String readTestResource(String fileName) {
        File file = new File(this.getClass().getResource(fileName).getFile());
        try {
            return Files.asCharSource(file, Charsets.UTF_8).read();
        } catch (IOException e) {
            throw new RuntimeException("Unable to read test resource: " + fileName, e);
        }
    }
}