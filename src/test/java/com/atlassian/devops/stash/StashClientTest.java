package com.atlassian.devops.stash;

import com.atlassian.devops.Config;
import org.junit.Test;


public class StashClientTest {

    private StashClient client = new StashClient(new Config(), new StashResponseParser());

    @Test
    public void testListMergedPullRequests() {
        client.listMergedPullRequests("STASH", "stash", null);
    }

    @Test
    public void testListPullRequestCommits() {
        client.listPullRequestCommits("STASH", "stash", 12942, null);
    }

    @Test
    public void testListPullRequestActivities() {
        client.listPullRequestActivities("STASH", "stash", 12942, null);
    }
}