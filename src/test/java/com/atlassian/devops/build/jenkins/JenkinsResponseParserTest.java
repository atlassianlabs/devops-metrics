package com.atlassian.devops.build.jenkins;

import com.atlassian.devops.build.BuildResult;
import com.atlassian.devops.util.Page;
import com.google.common.base.Charsets;
import com.google.common.io.Files;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

public class JenkinsResponseParserTest {

    private JenkinsResponseParser responseParser = new JenkinsResponseParser();

    @Test
    public void testShouldParseBambooAllBuildsListData() {
        String buildResultsJson = readTestResource("jenkins-all-builds-result-list.json");

        Page<BuildResult> page = responseParser.parseBuildResultsList(buildResultsJson);

        assertNotNull(page);
        assertEquals(0, page.getStart());
        assertEquals(3, page.getLimit());
        assertEquals(3, page.getSize());
        assertTrue(page.isLastPage());

        List<BuildResult> buildResults = page.getValues();
        assertNotNull(buildResults);
        assertEquals(3, buildResults.size());

        BuildResult result = buildResults.get(1);
        assertEquals(2668L, result.getBuildDurationMs());
        assertEquals(1549843869146L, result.getStartTime().getTime());
        assertEquals(1, result.getChanges().size());
        assertEquals("8a8bc9bad6405a020d6e3dc1c5e27d7831f47aef", result.getChanges().get(0));
    }

    @Test
    public void testShouldParseBambooBuildsListDataWithIncompleteBuild() {
        // Sanity test that we _also_ handle the "builds" tag (instead of "allBuilds")
        String buildResultsJson = readTestResource("jenkins-builds-result-list-with-incomplete-build.json");

        Page<BuildResult> page = responseParser.parseBuildResultsList(buildResultsJson);

        assertEquals(4, page.getValues().size());
        // This test data has an in-progress build, so check that out...
        assertFalse(page.getValues().get(0).isComplete());
        assertTrue(page.getValues().get(1).isComplete());
        assertTrue(page.getValues().get(2).isComplete());
        assertTrue(page.getValues().get(3).isComplete());
    }

    private String readTestResource(String fileName) {
        File file = new File(this.getClass().getResource(fileName).getFile());
        try {
            return Files.asCharSource(file, Charsets.UTF_8).read();
        } catch (IOException e) {
            throw new RuntimeException("Unable to read test resource: " + fileName, e);
        }
    }
}
