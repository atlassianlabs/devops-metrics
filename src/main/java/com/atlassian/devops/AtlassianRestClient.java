package com.atlassian.devops;

public class AtlassianRestClient extends RestClient {

    public AtlassianRestClient(String product_url, String username, String password) {
        super(product_url + "/rest/api/latest", username, password);
    }
}
