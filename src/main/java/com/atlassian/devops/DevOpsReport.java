package com.atlassian.devops;

import com.atlassian.devops.build.BuildResult;
import com.atlassian.devops.pull.Commit;
import com.atlassian.devops.pull.PullRequest;

import java.util.List;
import java.util.Map;

public class DevOpsReport {
    private final int periodDays;
    private final List<BuildResult> buildsInPeriod;
    private final List<JiraIssue> incidentsRaisedInPeriod;
    private final List<JiraIssue> incidentsClosedInPeriod;
    private final List<PullRequest> pullRequestsMergedInPeriod;
    private final Map<PullRequest, Commit> pullRequestToFirstCommit;
    private final Map<PullRequest, BuildResult> pullRequestToDeployment;

    public DevOpsReport(int periodDays, List<BuildResult> buildsInPeriod, List<JiraIssue> incidentsRaisedInPeriod,
                        List<JiraIssue> incidentsClosedInPeriod, List<PullRequest> pullRequestsMergedInPeriod,
                        Map<PullRequest, Commit> pullRequestToFirstCommit, Map<PullRequest, BuildResult> pullRequestToDeployment) {
        this.periodDays = periodDays;
        this.buildsInPeriod = buildsInPeriod;
        this.incidentsRaisedInPeriod = incidentsRaisedInPeriod;
        this.incidentsClosedInPeriod = incidentsClosedInPeriod;
        this.pullRequestsMergedInPeriod = pullRequestsMergedInPeriod;
        this.pullRequestToFirstCommit = pullRequestToFirstCommit;
        this.pullRequestToDeployment = pullRequestToDeployment;
    }

    public int getPeriodDays() {
        return periodDays;
    }

    public List<BuildResult> getBuildsInPeriod() {
        return buildsInPeriod;
    }

    public List<JiraIssue> getIncidentsRaisedInPeriod() {
        return incidentsRaisedInPeriod;
    }

    public List<JiraIssue> getIncidentsClosedInPeriod() {
        return incidentsClosedInPeriod;
    }

    public List<PullRequest> getPullRequestsMergedInPeriod() {
        return pullRequestsMergedInPeriod;
    }

    public Map<PullRequest, Commit> getPullRequestToFirstCommit() {
        return pullRequestToFirstCommit;
    }

    public Map<PullRequest, BuildResult> getPullRequestToDeployment() {
        return pullRequestToDeployment;
    }
}
