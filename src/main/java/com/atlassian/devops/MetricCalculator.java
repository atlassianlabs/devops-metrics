package com.atlassian.devops;

import com.atlassian.devops.build.BuildResult;
import com.atlassian.devops.build.BuildService;
import com.atlassian.devops.jira.JiraClient;
import com.atlassian.devops.pull.Commit;
import com.atlassian.devops.pull.PullRequest;
import com.atlassian.devops.pull.PullRequestActivity;
import com.atlassian.devops.stash.StashClient;
import com.atlassian.devops.util.Page;
import com.atlassian.devops.util.PageRequest;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.math.Stats;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

public class MetricCalculator {

    private static final Logger log = LoggerFactory.getLogger(MetricCalculator.class);

    private static final long MS_IN_A_DAY = 24 * 60 * 60 * 1000;

    private final String buildKey;
    private final String incidentJql;
    private final JiraClient jiraClient;
    private final int periodDays;
    private final StashClient stashClient;
    private final String stashProject;
    private final String stashRepository;
    private final BuildService buildService;

    public MetricCalculator(BuildService buildService, Config config, JiraClient jiraClient, StashClient stashClient) {
        this.buildService = buildService;
        this.jiraClient = jiraClient;
        this.stashClient = stashClient;

        this.buildKey = config.getRequiredProperty("build.key");
        this.incidentJql = config.getRequiredProperty("incident.jql");
        this.periodDays = Integer.parseInt(config.getRequiredProperty("period.days"));
        this.stashProject = config.getRequiredProperty("stash.project");
        this.stashRepository = config.getRequiredProperty("stash.repository");
    }

    public DevOpsReport createDevOpsReport() {
        log.info("Calculating devops metrics for last {} days", periodDays);

        log.info("Finding builds...");
        List<BuildResult> buildsInPeriod = getBuildsInPeriod();

        log.info("Finding incidents raised...");
        List<JiraIssue> incidentsRaisedInPeriod = getIncidentsRaisedInPeriod();

        log.info("Finding incidents closed...");
        List<JiraIssue> incidentsClosedInPeriod = getIncidentsClosedInPeriod();

        log.info("Finding merged pull requests...");
        List<PullRequest> pullRequestsMergedInPeriod = getPullRequestsMergedInPeriod();

        log.info("Finding first commit for each pull request...");
        Map<PullRequest, Commit> pullRequestsToFirstCommit = getFirstCommitForEachPullRequest(pullRequestsMergedInPeriod);

        log.info("Linking pull requests to builds...");
        Map<PullRequest, BuildResult> pullRequestToDeployment = getDeploymentForEachPullRequest(pullRequestsMergedInPeriod, buildsInPeriod);

        log.info("\n\n\n");
        return new DevOpsReport(periodDays, buildsInPeriod, incidentsRaisedInPeriod, incidentsClosedInPeriod,
                pullRequestsMergedInPeriod, pullRequestsToFirstCommit, pullRequestToDeployment);
    }

    public void displayDevOpsMetrics(DevOpsReport report) {
        log.info("In the last {} days:", report.getPeriodDays());
        log.info("--------------------------");
        log.info("Incidents raised:             {}", report.getIncidentsRaisedInPeriod().size());
        log.info("Incidents resolved:           {}", report.getIncidentsClosedInPeriod().size());
        log.info("Pull requests merged:         {}", report.getPullRequestsMergedInPeriod().size());
        log.info("");
        log.info("Deployment frequency:         {}", report.getBuildsInPeriod().size());
        log.info("Change failure rate:          {}", formatDouble(report.getIncidentsRaisedInPeriod().size() / (double) report.getBuildsInPeriod().size()));
        log.info("Mean time to recovery (days): {}", formatDouble(calculateMeanTimeToRecoveryInDays(report.getIncidentsClosedInPeriod())));
        log.info("");
        log.info("Change lead time: (min, avg, max)");
        log.info("---------------------------------");
        log.info("Development (days): {}", displayStats(calculateDevelopmentTimesInDays(report.getPullRequestToFirstCommit())));
        log.info("Review (days):      {}", displayStats(calculateReviewTimesInDays(report.getPullRequestsMergedInPeriod())));
        log.info("Idle time (mins):   {}", displayStats(calculateIdleTimeInMinutes(report.getPullRequestToDeployment())));
        log.info("Deployment (mins):  {}", displayStats(calculateDeploymentLeadTimesInMins(report.getBuildsInPeriod())));
    }

    private String formatDouble(double v) {
        return String.format("%1$.2f", v);
    }

    private String displayStats(List<Double> statsInDays) {
        Stats stats = Stats.of(statsInDays);
        return String.format("%s / %s / %s", formatDouble(stats.min()), formatDouble(stats.mean()),
                formatDouble(stats.max()));
    }

    private List<Double> calculateDeploymentLeadTimesInMins(List<BuildResult> builds) {
        return builds.stream().map(build ->
                build.getBuildDurationMs() / (double) 60 / 1000
        ).collect(Collectors.toList());
    }

    private List<Double> calculateDevelopmentTimesInDays(Map<PullRequest, Commit> pullRequestsToFirstCommit) {
        return pullRequestsToFirstCommit.entrySet().stream().map(entry ->
                (entry.getKey().getCreatedDate().getTime() - entry.getValue().getAuthorTimestamp().getTime()) / (double) MS_IN_A_DAY
        ).collect(Collectors.toList());
    }

    private List<Double> calculateIdleTimeInMinutes(Map<PullRequest, BuildResult> pullRequestToDeployment) {
        return pullRequestToDeployment.entrySet().stream().map(entry ->
                (entry.getValue().getStartTime().getTime() - entry.getKey().getClosedDate().getTime()) / (double) 60 / 1000
        ).collect(Collectors.toList());
    }

    private double calculateMeanTimeToRecoveryInDays(List<JiraIssue> incidentsClosed) {
        return Stats.of(incidentsClosed.stream().map(incident -> {
            Date resolutionDate = incident.getResolutionDate().orElseThrow(() ->
                    new RuntimeException("Resolution date is required to calculate mean time to recovery"));
            return (resolutionDate.getTime() - incident.getCreatedDate().getTime()) / (double) MS_IN_A_DAY;
        }).collect(Collectors.toList()))
                .mean();
    }

    private List<Double> calculateReviewTimesInDays(List<PullRequest> pullRequests) {
        return pullRequests.stream().map(pullRequest ->
                (pullRequest.getClosedDate().getTime() - pullRequest.getCreatedDate().getTime()) / (double) MS_IN_A_DAY)
                .collect(Collectors.toList());
    }

    private List<BuildResult> getBuildsInPeriod() {
        return buildService.findCompleteBuildsSince(buildKey, getStartOfPeriod().getTime());
    }

    private Map<PullRequest, BuildResult> getDeploymentForEachPullRequest(List<PullRequest> pullRequests,
                                                                          List<BuildResult> deployments) {
        ImmutableMap.Builder<String, BuildResult> commitToDeploymentBuilder = ImmutableMap.builder();
        for (BuildResult deployment : deployments) {
            for (String commit : deployment.getChanges()) {
                commitToDeploymentBuilder.put(commit, deployment);
            }
        }
        Map<String, BuildResult> commitToBuild = commitToDeploymentBuilder.build();

        ImmutableMap.Builder<PullRequest, BuildResult> pullRequestToDeploymentBuilder = ImmutableMap.builder();
        for (PullRequest pullRequest : pullRequests) {
            Optional<String> mergeCommit = getMergeCommitForPullRequest(pullRequest.getPullRequestId());
            if (mergeCommit.isPresent() && commitToBuild.get(mergeCommit.get()) != null) {
                pullRequestToDeploymentBuilder.put(pullRequest, commitToBuild.get(mergeCommit.get()));
            }
        }
        return pullRequestToDeploymentBuilder.build();
    }

    private Commit getFirstCommitForPullRequest(int pullRequestId) {
        Commit earlistCommit = null;

        PageRequest request = null;
        do {
            Page<Commit> commits = stashClient.listPullRequestCommits(stashProject, stashRepository, pullRequestId, request);
            for (Commit commit : commits.getValues()) {
                if (earlistCommit == null || earlistCommit.getAuthorTimestamp().compareTo(commit.getAuthorTimestamp()) > 0) {
                    earlistCommit = commit;
                }
            }
            request = commits.getNextPageRequest();
        } while (request != null);

        return earlistCommit;
    }

    private Map<PullRequest, Commit> getFirstCommitForEachPullRequest(List<PullRequest> pullRequests) {
        ImmutableMap.Builder<PullRequest, Commit> mapBuilder = ImmutableMap.builder();
        for (PullRequest pullRequest : pullRequests) {
            mapBuilder.put(pullRequest, getFirstCommitForPullRequest(pullRequest.getPullRequestId()));
        }

        return mapBuilder.build();
    }

    private List<JiraIssue> getIncidentsClosedInPeriod() {
        String incidentsJql = String.format("%s AND resolutiondate >= -%dd", this.incidentJql, periodDays);
        return queryJiraIssues(incidentsJql);
    }

    private List<JiraIssue> getIncidentsRaisedInPeriod() {
        String incidentsJql = String.format("%s AND created >= -%dd", this.incidentJql, periodDays);
        return queryJiraIssues(incidentsJql);
    }

    private Optional<String> getMergeCommitForPullRequest(int pullRequestId) {
        PageRequest request = null;
        do {
            Page<PullRequestActivity> activities = stashClient.listPullRequestActivities(stashProject, stashRepository, pullRequestId, request);
            for (PullRequestActivity activity : activities.getValues()) {
                if (activity.getAction().equals("MERGED")) {
                    return activity.getCommitId();
                }
            }
            request = activities.getNextPageRequest();
        } while (request != null);

        return Optional.empty();
    }

    private List<PullRequest> getPullRequestsMergedInPeriod() {
        ImmutableList.Builder<PullRequest> pullRequestsInPeriod = ImmutableList.builder();
        Date startOfPeriod = getStartOfPeriod();

        PageRequest request = null;
        do {
            Page<PullRequest> pullRequests = stashClient.listMergedPullRequests(stashProject, stashRepository, request);
            for (PullRequest pullRequest : pullRequests.getValues()) {
                if (pullRequest.getClosedDate().compareTo(startOfPeriod) > 0) {
                    pullRequestsInPeriod.add(pullRequest);
                } else {
                    return pullRequestsInPeriod.build();
                }
            }

            request = pullRequests.getNextPageRequest();
        } while (request != null);

        return pullRequestsInPeriod.build();
    }

    private Date getStartOfPeriod() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, -periodDays);
        return cal.getTime();
    }

    private List<JiraIssue> queryJiraIssues(String incidentsJql) {
        ImmutableList.Builder<JiraIssue> incidentsRaisedInPeriod = ImmutableList.builder();

        PageRequest request = null;
        do {
            Page<JiraIssue> jiraIssuePage = jiraClient.search(incidentsJql, request);
            incidentsRaisedInPeriod.addAll(jiraIssuePage.getValues());
            request = jiraIssuePage.getNextPageRequest();
        } while (request != null);

        return incidentsRaisedInPeriod.build();
    }
}
