package com.atlassian.devops;

import com.google.common.base.MoreObjects;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Date;
import java.util.Optional;

public class JiraIssue {

    private final Date createdDate;
    private final String issueKey;
    private final Date resolutionDate;

    public JiraIssue(@Nonnull String issueKey, @Nonnull Date createdDate, @Nullable Date resolutionDate) {
        this.createdDate = createdDate;
        this.issueKey = issueKey;
        this.resolutionDate = resolutionDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        JiraIssue issue = (JiraIssue) o;
        return createdDate.equals(issue.getCreatedDate()) &&
                issueKey.equals(issue.getIssueKey()) &&
                getResolutionDate().equals(issue.getResolutionDate());
    }

    @Nonnull
    public Date getCreatedDate() {
        return createdDate;
    }

    @Nonnull
    public String getIssueKey() {
        return issueKey;
    }

    @Nonnull
    public Optional<Date> getResolutionDate() {
        return Optional.ofNullable(resolutionDate);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("issueKey", issueKey)
                .toString();
    }
}
