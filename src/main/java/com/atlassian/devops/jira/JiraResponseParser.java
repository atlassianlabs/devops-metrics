package com.atlassian.devops.jira;

import com.atlassian.devops.JiraIssue;
import com.atlassian.devops.util.Page;
import com.atlassian.devops.util.PageImpl;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class JiraResponseParser {

    private final SimpleDateFormat jiraDateFormatter;

    public JiraResponseParser() {
        jiraDateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        jiraDateFormatter.setLenient(false);
    }

    @Nonnull
    public Page<JiraIssue> parseJiraIssueList(String json) {

        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = mapper.readTree(json);

            int startIndex = node.get("startAt").asInt();
            int maxResultsRequested = node.get("maxResults").asInt();

            ImmutableList.Builder<JiraIssue> issueListBuilder = ImmutableList.builder();
            for (JsonNode issue : node.get("issues")) {
                String issueKey = issue.get("key").asText();

                Date createdDate = jiraDateFormatter.parse(issue.get("fields").get("created").asText());
                String resolved = issue.get("fields").get("resolutiondate").asText();
                Date resolvedDate = resolved.equals("null") ? null : jiraDateFormatter.parse(resolved);

                issueListBuilder.add(new JiraIssue(issueKey, createdDate, resolvedDate));
            }

            List<JiraIssue> issues = issueListBuilder.build();

            return new PageImpl<>(startIndex, maxResultsRequested, issues, maxResultsRequested > issues.size());
        } catch (IOException | ParseException e) {
            throw new RuntimeException("Error parsing Bamboo build results list", e);
        }
    }
}
