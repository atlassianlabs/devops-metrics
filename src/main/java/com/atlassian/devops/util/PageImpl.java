package com.atlassian.devops.util;

import javax.annotation.Nonnull;
import java.util.List;

public class PageImpl<T> implements Page<T> {

    private final boolean lastPage;
    private final int nextPageStart;
    private final int start;
    private final int limit;
    private final int size;
    private final List<T> values;

    public PageImpl(int start, int limit, int size, List<T> values, boolean lastPage) {
        this(start, limit, values, lastPage, size, start + limit);
    }

    public PageImpl(int start, int limit, List<T> values, boolean lastPage, int size, int nextPageStart) {
        this.lastPage = lastPage;
        this.nextPageStart = nextPageStart;
        this.start = start;
        this.limit = limit;
        this.size = size;
        this.values = values;
    }

    public PageImpl(int start, int limit, List<T> values, boolean lastPage) {
        this(start, limit, values.size(), values, lastPage);
    }

    @Override
    public boolean isLastPage() {
        return lastPage;
    }

    @Override
    public int getLimit() {
        return limit;
    }

    @Override
    public PageRequest getNextPageRequest() {
        return isLastPage() ? null : new PageRequestImpl(getNextPageOffset(), getLimit());
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public int getStart() {
        return start;
    }

    @Nonnull
    @Override
    public List<T> getValues() {
        return values;
    }

    protected int getNextPageOffset() {
        return nextPageStart;
    }
}
