package com.atlassian.devops.util;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

public interface Page<T> {

    int getStart();

    int getSize();

    int getLimit();

    boolean isLastPage();

    @Nonnull
    List<T> getValues();

    @Nullable
    PageRequest getNextPageRequest();
}

