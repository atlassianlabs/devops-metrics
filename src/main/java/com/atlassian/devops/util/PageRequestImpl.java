package com.atlassian.devops.util;

/**
 * Default implementation of {@link PageRequest}.
 */
public class PageRequestImpl implements PageRequest {

    private final int start;
    private final int limit;

    public PageRequestImpl(int start, int limit) {
        this.start = start < 0 ? 0 : start;
        this.limit = limit <= 0 ? 1 : limit;
    }

    @Override
    public int getStart() {
        return start;
    }

    @Override
    public int getLimit() {
        return limit;
    }
}
