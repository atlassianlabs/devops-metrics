package com.atlassian.devops;

import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

public abstract class RestClient {

    private static final Logger log = LoggerFactory.getLogger(RestClient.class);

    protected final WebTarget webTarget;

    public RestClient(String product_url, String username, String password) {
        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(username, password);
        Client client = ClientBuilder.newClient().register(feature);
        webTarget = client.target(product_url);
    }

    protected String getAsString(Invocation.Builder invocationBuilder) {
        Response response = invocationBuilder.get();
        String responseJson = response.readEntity(String.class);
        log.debug("Status: {}", response.getStatus());
        log.debug("Content: {}", responseJson);
        return responseJson;
    }
}
