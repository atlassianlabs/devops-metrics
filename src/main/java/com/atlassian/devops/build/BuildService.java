package com.atlassian.devops.build;

import java.util.List;

public interface BuildService {

    List<BuildResult> findCompleteBuildsSince(String buildKey, long firstTimeStamp);
}
