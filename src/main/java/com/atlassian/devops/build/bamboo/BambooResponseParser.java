package com.atlassian.devops.build.bamboo;

import com.atlassian.devops.build.BuildResult;
import com.atlassian.devops.util.Page;
import com.atlassian.devops.util.PageImpl;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class BambooResponseParser {

    private final SimpleDateFormat bambooDateFormatter;

    public BambooResponseParser() {
        bambooDateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        bambooDateFormatter.setLenient(false);
        bambooDateFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    @Nonnull
    public Page<BuildResult> parseBuildResultsList(String json) {

        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = mapper.readTree(json);

            JsonNode results = node.get("results");

            int numResultsReturned = results.get("size").asInt();
            int startIndex = results.get("start-index").asInt();
            int maxResultsRequested = results.get("max-result").asInt();

            ImmutableList.Builder<BuildResult> buildListBuilder = ImmutableList.builder();
            for (JsonNode result : results.get("result")) {
                buildListBuilder.add(parseBuildResult(result));
            }

            List<BuildResult> builds = buildListBuilder.build();

            return new PageImpl<>(startIndex, maxResultsRequested, builds, maxResultsRequested > numResultsReturned);
        } catch (IOException e) {
            throw new RuntimeException("Error parsing Bamboo build results list", e);
        }
    }

    @Nonnull
    public BuildResult parseBuildResult(String json) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = mapper.readTree(json);
            return parseBuildResult(node);
        } catch (IOException e) {
            throw new RuntimeException("Error parsing Bamboo build results list", e);
        }
    }

    @Nonnull
    private BuildResult parseBuildResult(JsonNode json) {
        try {
            String buildResultKey = json.get("buildResultKey").asText();
            Date buildStartedTime = bambooDateFormatter.parse(json.get("buildStartedTime").asText());
            long buildDurationMs = json.get("buildDuration").asLong();
            // Bamboo only returns complete builds
            boolean isComplete = true;

            ImmutableList.Builder<String> changes = ImmutableList.builder();

            for (JsonNode change : json.at("/changes/change")) {
                changes.add(change.get("changesetId").asText());
            }

            return new BuildResult(buildResultKey, buildStartedTime, buildDurationMs, changes.build(), isComplete);
        } catch (ParseException e) {
            throw new RuntimeException("Error parsing Bamboo build result", e);
        }
    }
}
