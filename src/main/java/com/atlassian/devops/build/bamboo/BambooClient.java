package com.atlassian.devops.build.bamboo;

import com.atlassian.devops.AtlassianRestClient;
import com.atlassian.devops.Config;
import com.atlassian.devops.build.BuildClient;
import com.atlassian.devops.build.BuildResult;
import com.atlassian.devops.util.Page;
import com.atlassian.devops.util.PageRequest;
import com.atlassian.devops.util.PageRequestImpl;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.MediaType;

import static java.util.Objects.requireNonNull;

public class BambooClient extends AtlassianRestClient implements BuildClient {

    private final int defaultPageSize;
    private final BambooResponseParser responseParser;

    public BambooClient(Config config, BambooResponseParser responseParser) {
        super(config.getRequiredProperty("build.base.url"),
                config.getRequiredProperty("build.username"),
                config.getRequiredProperty("build.password"));

        defaultPageSize = Integer.parseInt(config.getRequiredProperty("build.pageSize"));
        this.responseParser = responseParser;
    }

    public Page<BuildResult> listBuildResults(@Nonnull String planKey, @Nullable PageRequest pageRequest) {
        requireNonNull(planKey, "planKey");
        if (pageRequest == null) {
            pageRequest = new PageRequestImpl(0, defaultPageSize);
        }

        Invocation.Builder invocationBuilder = webTarget.path("result").path(planKey)
                .queryParam("start-index", pageRequest.getStart())
                .queryParam("max-results", pageRequest.getLimit())
                .queryParam("expand", "results.result")
                .request(MediaType.APPLICATION_JSON_TYPE);

        String response = getAsString(invocationBuilder);
        return responseParser.parseBuildResultsList(response);
    }

    @Override
    public BuildResult enrichBuildCommitInfo(@Nonnull BuildResult buildResult) {
        // The changes associated with the build aren't available on the list, so we need to query each one individually
        String planResultKey = buildResult.getBuildId();
        requireNonNull(planResultKey, "planResultKey");
        Invocation.Builder invocationBuilder = webTarget.path("result").path(planResultKey)
                .queryParam("expand", "changes")
                .request(MediaType.APPLICATION_JSON_TYPE);
        String response = getAsString(invocationBuilder);
        return responseParser.parseBuildResult(response);
    }
}
