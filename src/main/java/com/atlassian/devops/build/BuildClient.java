package com.atlassian.devops.build;

import com.atlassian.devops.util.Page;
import com.atlassian.devops.util.PageRequest;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public interface BuildClient {

    Page<BuildResult> listBuildResults(@Nonnull String planKey, @Nullable PageRequest pageRequest);

    BuildResult enrichBuildCommitInfo(@Nonnull BuildResult buildResult);
}
