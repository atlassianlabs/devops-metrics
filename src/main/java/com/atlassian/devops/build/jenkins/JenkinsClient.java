package com.atlassian.devops.build.jenkins;

import com.atlassian.devops.Config;
import com.atlassian.devops.RestClient;
import com.atlassian.devops.build.BuildClient;
import com.atlassian.devops.build.BuildResult;
import com.atlassian.devops.util.Page;
import com.atlassian.devops.util.PageRequest;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.MediaType;

import static java.util.Objects.requireNonNull;

public class JenkinsClient extends RestClient implements BuildClient {

    private final JenkinsResponseParser responseParser;

    public JenkinsClient(Config config, JenkinsResponseParser responseParser) {
        super(config.getRequiredProperty("build.base.url"),
                config.getRequiredProperty("build.username"),
                config.getRequiredProperty("build.password"));

        this.responseParser = responseParser;
    }

    @Override
    public Page<BuildResult> listBuildResults(@Nonnull String planKey, @Nullable PageRequest pageRequest) {
        // Paging isn't supported because Jersey doesn't support Jenkins' non-standard URL format
        requireNonNull(planKey, "projectName");
        String queryString = "builds[result,building,number,duration,timestamp,changeSet[items[commitId]]]";
        Invocation.Builder invocationBuilder = webTarget.path("job").path(planKey).path("api/json")
                .queryParam("tree", queryString)
                .request(MediaType.APPLICATION_JSON_TYPE);
        String response = getAsString(invocationBuilder);
        return responseParser.parseBuildResultsList(response);
    }

    @Override
    public BuildResult enrichBuildCommitInfo(@Nonnull BuildResult buildResult) {
        // Nothing to do - Jenkins's results already include commit info
        return buildResult;
    }
}
