package com.atlassian.devops;

import com.atlassian.devops.build.BuildClient;
import com.atlassian.devops.build.BuildService;
import com.atlassian.devops.build.DefaultBuildService;
import com.atlassian.devops.build.bamboo.BambooClient;
import com.atlassian.devops.build.bamboo.BambooResponseParser;
import com.atlassian.devops.build.jenkins.JenkinsClient;
import com.atlassian.devops.build.jenkins.JenkinsResponseParser;
import com.atlassian.devops.jira.JiraClient;
import com.atlassian.devops.jira.JiraResponseParser;
import com.atlassian.devops.stash.StashClient;
import com.atlassian.devops.stash.StashResponseParser;

public class App {
    public static void main(String[] args) {
        Config config = new Config();

        String buildSystem = config.getProperty("build.system");
        BuildClient buildClient;
        if ("bamboo".equalsIgnoreCase(buildSystem)) {
            buildClient = new BambooClient(config, new BambooResponseParser());
        } else if ("jenkins".equalsIgnoreCase(buildSystem)) {
            buildClient = new JenkinsClient(config, new JenkinsResponseParser());
        } else {
            throw new RuntimeException("Unknown build system: " + buildSystem);
        }
        BuildService buildService = new DefaultBuildService(buildClient);

        JiraClient jiraClient = new JiraClient(config, new JiraResponseParser());

        StashClient stashClient = new StashClient(config, new StashResponseParser());

        MetricCalculator metricCalculator = new MetricCalculator(buildService, config, jiraClient, stashClient);

        DevOpsReport report = metricCalculator.createDevOpsReport();
        metricCalculator.displayDevOpsMetrics(report);
    }
}
