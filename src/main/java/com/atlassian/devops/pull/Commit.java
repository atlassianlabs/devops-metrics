package com.atlassian.devops.pull;

import javax.annotation.Nonnull;

import java.util.Date;

import static java.util.Objects.requireNonNull;

public class Commit {

    private final String commitId;
    private final Date authorTimestamp;

    public Commit(@Nonnull String commitId, @Nonnull Date authorTimestamp) {
        this.commitId = requireNonNull(commitId);
        this.authorTimestamp = requireNonNull(authorTimestamp);
    }

    public Date getAuthorTimestamp() {
        return authorTimestamp;
    }

    public String getCommitId() {
        return commitId;
    }
}
